﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotaterWall : MonoBehaviour
{
    //made public to make modifications easier
    public int rotationSpeed;

    // Update is called once per frame
    void Update()
    {
            //each frame, rotate according the given speed about the y-axis
            transform.Rotate(new Vector3(0, rotationSpeed, 0) * Time.deltaTime);
    }
}
