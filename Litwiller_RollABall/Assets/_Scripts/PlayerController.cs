﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    private int count;
    public Text countText;
    public Text winText;
    public int speedCount;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";

        //This counter keeps track of current frame
        speedCount = 0;
    }

    // Update is called once per frame
    private void Update()
    {
        //Each frame, check if it's time to modify the speed
        ModifySpeed();
    }

    void ModifySpeed()
    {
        //Add one to the speed counter then check if 120 frames have passed
        speedCount++;
        if (speedCount == 120)
        {
            //If 120 frames have passed since the last speed change, make another speed change
            System.Random rand = new System.Random();
            speed = (int)(rand.NextDouble() * 50);

            //need to reset the timer as well
            speedCount = 0;
        }
    }

    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        //If statements for each type of pickup to modify the score appropriately 
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
        else if (other.gameObject.CompareTag("Pick Up +2"))
        {
            other.gameObject.SetActive(false);
            count += 2;
            SetCountText();
        }
        else if (other.gameObject.CompareTag("Pick Up +5")) { 
            other.gameObject.SetActive(false);
            count += 5;
            SetCountText();
        }
    }


    void SetCountText()
    {
        //Added legend to show the point values for each color
        countText.text = "Score: " + count.ToString() + "\n\nYellow = +1\nGreen = +2\nPurple = +5";

        //updated count for new score max
        if(count >= 40)
        {
            winText.text = "You Win!";
        }
    }
}
